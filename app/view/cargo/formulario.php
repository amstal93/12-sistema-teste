<?php include __DIR__ . '/../inicio-html.php'; ?>

<div class="container-fluid p-0 d-flex justify-content-between">
    <span>
        <h1><?= $titulo; ?></h1>
    </span>
    <span>
        <?php if (!$formDisabled) { ?>
            <button type="submit" form="formulario-cargo" class="btn btn-primary">Salvar</button>
        <?php } ?>
        <button class="btn btn-secondary" onclick="document.location.href='/listar-cargos'">Cancelar</button>
    </span>

    
</div>

<form id="formulario-cargo" action="/salvar-cargo<?= isset($cargo) ? '?id=' . $cargo->getId() : ''; ?>" method="post">
    <fieldset class="form-group" <?= $formDisabled ? "disabled" : ""; ?>>
        <label for="descricao">Descrição</label>
        <input type="text" id="descricao" name="descricao" required class="form-control mb-3" value="<?= (isset($cargo) && $cargo->getDescricao() != null) ? $cargo->getDescricao() : ''; ?>">
        
        <label for="carga_horaria">Carga horária</label>
        <input type="number" id="carga_horaria" name="carga_horaria" required class="form-control mb-3" value="<?= (isset($cargo) && $cargo->getCargaHoraria() != null) ? $cargo->getCargaHoraria() : ''; ?>">

        <label for="dias_semana">Dias por semana</label>
        <input type="number" id="dias_semana" name="dias_semana" required class="form-control mb-3" value="<?= (isset($cargo) && $cargo->getDiasSemana() != null) ? $cargo->getDiasSemana() : ''; ?>">
    </fieldset>
</form>

<?php include __DIR__ . '/../fim-html.php'; ?>