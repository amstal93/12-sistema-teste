<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126162337 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Funcionario (id INT AUTO_INCREMENT NOT NULL, cargo_id INT DEFAULT NULL, lotacao_id INT DEFAULT NULL, nome VARCHAR(255) NOT NULL, login VARCHAR(255) NOT NULL, senha VARCHAR(255) NOT NULL, matricula INT NOT NULL, horario_inicio TIME NOT NULL, horario_fim TIME NOT NULL, INDEX IDX_F38C2C18813AC380 (cargo_id), INDEX IDX_F38C2C187E27C2B8 (lotacao_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Funcionario ADD CONSTRAINT FK_F38C2C18813AC380 FOREIGN KEY (cargo_id) REFERENCES Cargo (id)');
        $this->addSql('ALTER TABLE Funcionario ADD CONSTRAINT FK_F38C2C187E27C2B8 FOREIGN KEY (lotacao_id) REFERENCES Lotacao (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE Funcionario');
    }
}
