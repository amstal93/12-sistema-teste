<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Menu
{
    /**
     * @Id
     * @GeneratedValue
     * @Column (type="integer")
     */
    private $id;
    /**
     * @Column (type="string")
     */
    private $descricao;
    /**
     * @Column (type="string", nullable=true)
     */
    private $alias;
    /**
     * @ManyToMany (targetEntity="Funcionario", mappedBy="autorizacoes")
     */
    private $autorizados;

    public function __construct()
    {
        $this->autorizados = new ArrayCollection();
    }

    public function addAutorizado(Funcionario $funcionario)
    {
        $this->autorizados->add($funcionario);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = $descricao;
        return $this;
    }
    
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }
}