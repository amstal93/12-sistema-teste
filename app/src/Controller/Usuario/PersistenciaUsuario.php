<?php

namespace App\Controller\Usuario;

use App\Entity\Cargo;
use App\Entity\Funcionario;
use App\Entity\Lotacao;
use App\Entity\Menu;
use App\Helper\FlashMessageTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PersistenciaUsuario implements RequestHandlerInterface
{
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioCargos;
    private $repositorioLotacoes;
    private $repositorioUsuarios;
    private $repositorioDeMenus;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioCargos = $entityManager->getRepository(Cargo::class);
        $this->repositorioLotacoes = $entityManager->getRepository(Lotacao::class);
        $this->repositorioUsuarios = $entityManager->getRepository(Funcionario::class);
        $this->repositorioDeMenus = $entityManager->getRepository(Menu::class);
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $bodyString = $request->getParsedBody();
        $nome = filter_var($bodyString['nome'], FILTER_SANITIZE_STRING);
        $matricula = filter_var($bodyString['matricula'], FILTER_SANITIZE_NUMBER_INT);
        $login = filter_var($bodyString['login'], FILTER_SANITIZE_STRING);
        $senha = filter_var($bodyString['senha'], FILTER_SANITIZE_STRING);
        $horarioInicio = $this->validateDate($bodyString['horario-inicio'], 'H:i') ? DateTime::createFromFormat('H:i', $bodyString['horario-inicio']) : DateTime::createFromFormat('H:i', '0:00');
        $horarioFim = $this->validateDate($bodyString['horario-fim'], 'H:i') ? DateTime::createFromFormat('H:i', $bodyString['horario-fim']) : DateTime::createFromFormat('H:i', '0:00');
        $idCargo = filter_var($bodyString['cargo'], FILTER_SANITIZE_NUMBER_INT);
        $idLotacao = filter_var($bodyString['lotacao'], FILTER_SANITIZE_NUMBER_INT);
        $idArrayAutorizacoes = $bodyString['autorizacoes'];

        if (!is_null($idCargo) && $idCargo !== false) {
            $cargo = $this->repositorioCargos->find($idCargo);
        }

        if (!is_null($idLotacao) && $idLotacao !== false) {
            $lotacao = $this->repositorioLotacoes->find($idLotacao);
        }

        if (!is_null($idArrayAutorizacoes) && count($idArrayAutorizacoes) > 0) {
            $autorizacoes = [];
            foreach ($idArrayAutorizacoes as $idMenu) {
                $menu = $this->repositorioDeMenus->find($idMenu);
                array_push($autorizacoes, $menu);
            }
        }

        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (!is_null($idEntidade) && $idEntidade !== false) {
            # PUT request
            $entity = $this->repositorioUsuarios->find($idEntidade);
            $entity->setNome($nome);
            $entity->setMatricula($matricula);
            $entity->setLogin($login);
            // $entity->setSenha($senha);
            $entity->setHorarioInicio($horarioInicio);
            $entity->setHorarioFim($horarioFim);
            $entity->setCargo($cargo);
            $entity->setLotacao($lotacao);

            if (!is_null($senha)) {
                $entity->setSenha($senha);
            }

            if (!is_null($entity->getAutorizacoes()) && count($entity->getAutorizacoes()) > 0) {
                foreach ($entity->getAutorizacoes() as $menuAntigo) {

                    if (!is_null($autorizacoes) && count($autorizacoes) > 0) {
                        if (array_search($menuAntigo, $autorizacoes) === false) {
                            $entity->getAutorizacoes()->removeElement($menuAntigo);
                        } 

                    } else {
                        $entity->getAutorizacoes()->removeElement($menuAntigo);
                    }
                }
            }

            if (!is_null($autorizacoes) && count($autorizacoes) > 0) {
                foreach ($autorizacoes as $menuNovo) {
                    if (!$entity->getAutorizacoes()->contains($menuNovo)) {
                        $entity->addAutorizacao($menuNovo);
                    }
                }
            }

            $this->defineMensagem('success', 'Usuário alterado com sucesso.');

        } else {
            # POST request
            $usuario = new Funcionario();
            $usuario->setNome($nome);
            $usuario->setMatricula($matricula);
            $usuario->setLogin($login);
            $usuario->setSenha($senha);
            $usuario->setHorarioInicio($horarioInicio);
            $usuario->setHorarioFim($horarioFim);
            $usuario->setCargo($cargo);
            $usuario->setLotacao($lotacao);

            if (!is_null($autorizacoes) && count($autorizacoes) > 0) {
                foreach ($autorizacoes as $menu) {
                    $usuario->addAutorizacao($menu);
                }
            }

            $this->entityManager->persist($usuario);
            $this->defineMensagem('success', 'Usuário criado com sucesso.');
        }

        $this->entityManager->flush();
        return new Response(200, ['Location' => '/listar-usuarios']);
    }
}