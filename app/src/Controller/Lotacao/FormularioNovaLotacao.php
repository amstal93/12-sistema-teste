<?php

namespace App\Controller\Lotacao;

use App\Entity\Cidade;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FormularioNovaLotacao implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    private $entityManager;
    private $repositorioDeCidades;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeCidades = $entityManager->getRepository(Cidade::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $cidades = $this->repositorioDeCidades->findAll();

        $html = $this->renderizaHtml('lotacao/formulario.php', [
            'titulo' => 'Nova lotação',
            'cidades' => $cidades
        ]);
        return new Response(200, [], $html);
    }
}